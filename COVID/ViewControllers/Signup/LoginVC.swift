//
//  LoginVC.swift
//  COVID
//
//  Created by macOS on 01/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {

    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //txtEmailAddress.text = "covid1@gmail.com"
        //txtPassword.text = "123456"
    }
    
    //MARK:- UIBUTTON ACTION
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnLogin(_ sender: Any) {
        if validateEmailAddress(txtEmailAddress, withMessage: "Valid email address is required.") && validateTxtFieldLength(txtPassword, withMessage: "Password is required.") {
            LogIn()
        }
    }
}

//MARK:- LOGIN WITH FIREBASE
extension LoginVC {
    func LogIn() {
        self.view.endEditing(true)
        let loginManager = FirebaseAuthManager()
        guard let email = txtEmailAddress.text, let password = txtPassword.text else { return }
        showLoaderHUD()
        loginManager.signIn(email: email, pass: password) {[weak self] (success, user, error) in
            hideLoaderHUD()
            guard let `self` = self else { return }
            if (success) {
                print(user?.displayName ?? "name not found")
                print(user?.uid ?? "")
                let vc = loadVC(strStoryboardId: SB_BLUETOOTH, strVCId: "TabBarVC")
                self.navigationController?.pushViewController(vc, animated: true)
            } else {
                let alertController = UIAlertController(title: nil, message: error?.localizedDescription ?? "There was an error.", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
