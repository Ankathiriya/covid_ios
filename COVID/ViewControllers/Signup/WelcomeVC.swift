//
//  WelcomeVC.swift
//  COVID
//
//  Created by macOS on 01/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class WelcomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnLogin(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "LoginVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnSignUp(_ sender: Any) {
        let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "SignUpVC")
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
