//
//  SignUpVC.swift
//  COVID
//
//  Created by macOS on 01/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignUpVC: UIViewController {

    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtEmailAddress: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- UIBUTTON ACTIONS
    @IBAction func btnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnCreateAccount(_ sender: Any) {
        if validateTxtFieldLength(txtFullName, withMessage: "Full Name is required.") && validateEmailAddress(txtEmailAddress, withMessage: "Valid email address is required.") && validateTxtFieldLength(txtPassword, withMessage: "Password is required.") {
            SignUp()
        }
    }
}

//MARK:- SIGN UP WITH FIREBASE
extension SignUpVC {
    func SignUp() {
        self.view.endEditing(true)
        let signUpManager = FirebaseAuthManager()
        if let email = txtEmailAddress.text, let password = txtPassword.text {
            showLoaderHUD()
            signUpManager.createUser(email: email, password: password) {[weak self] (success, user, error) in
                hideLoaderHUD()
                guard let `self` = self else { return }
                var message: String = ""
                if (success) {
                    message = "User was sucessfully created."
                    let changeRequest = user?.createProfileChangeRequest()
                    changeRequest?.displayName = self.txtFullName.text ?? ""
                    changeRequest?.commitChanges { error in
                        if error != nil {
                            print("Error on update: ", error?.localizedDescription ?? "")
                        } else {
                            print("User successfully updated")
                        }
                    }
                } else {
                    message = error?.localizedDescription ?? "There was an error."
                }
                let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "OK", style: .cancel) { (action) in
                    if success { self.navigationController?.popViewController(animated: true) }
                })
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
}
