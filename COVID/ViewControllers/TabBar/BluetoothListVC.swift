//
//  BluetoothListVC.swift
//  COVID
//
//  Created by macOS on 04/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class CellBluetooth: UITableViewCell {
    @IBOutlet weak var lblBluetoothName: UILabel!
    @IBOutlet weak var lblUniqueNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}

class BluetoothListVC: UIViewController {
    
    @IBOutlet weak var tblBluetooth: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var arrBluetooth = NSMutableArray()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activity.startAnimating()
        refreshControl.addTarget(self, action: #selector(GetBluetoothList), for: .valueChanged)
        tblBluetooth.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetBluetoothList()
    }
}

//MARK:- UITABLEVIEWCELL DELEGATE METHOD
extension BluetoothListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrBluetooth.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellBluetooth = tableView.dequeueReusableCell(withIdentifier: "CellBluetooth", for: indexPath) as! CellBluetooth
        let objBluetooth: CBluetooth = arrBluetooth[indexPath.row] as! CBluetooth
        cell.lblBluetoothName.text = objBluetooth.name
        cell.lblUniqueNo.text = "\(objBluetooth.address ?? "")\n\(objBluetooth.lattitude ?? 0), \(objBluetooth.longitude ?? 0)\n\(objBluetooth.devicetype ?? ""): \(objBluetooth.deviceuniqueid ?? "")"
        cell.lblDate.text = objBluetooth.date
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- GET BLUETOOTH LIST
extension BluetoothListVC {
     @objc func GetBluetoothList() {
        let ref = MainRef().child(BLUETOOTH)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            self.refreshControl.endRefreshing()
            self.activity.stopAnimating()
            if !snapshot.exists() { return }
            self.arrBluetooth.removeAllObjects()
            self.tblBluetooth.reloadData()
            let dictAllBluetooth = snapshot.value! as! NSDictionary
            let arrAllKeys = dictAllBluetooth.allKeys as NSArray
            for i in 0..<arrAllKeys.count {
                let sKey = arrAllKeys[i] as! String
                let dictBluetooth: NSDictionary = dictAllBluetooth.object(forKey: sKey) as! NSDictionary
                print(dictBluetooth)
                let objBluetooth: CBluetooth = CBluetooth(object: dictBluetooth)
                if !self.arrBluetooth.contains(objBluetooth) {
                    self.arrBluetooth.add(objBluetooth)
                }
            }
            let sorted = self.arrBluetooth.sorted {
                guard let first = ($0 as! CBluetooth).date else { return false }
                guard let second = ($1 as! CBluetooth).date else { return true }
                return first.localizedCaseInsensitiveCompare(second) == ComparisonResult.orderedAscending
            }
            self.arrBluetooth.removeAllObjects()
            for obj in sorted {
                self.arrBluetooth.insert(obj, at: 0)
            }
            self.tblBluetooth.reloadData()
        })
    }
}
