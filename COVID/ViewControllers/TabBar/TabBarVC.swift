//
//  TabBarVC.swift
//  COVID
//
//  Created by macOS on 04/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class TabBarVC: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        UITabBar.appearance().tintColor = Color_Hex(hex: "FF5A66")
    }
    
}
