//
//  AddressListVC.swift
//  COVID
//
//  Created by macOS on 04/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class CellAddress: UITableViewCell {
    @IBOutlet weak var lblAddress: UILabel!
    @IBOutlet weak var lblLatLong: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}

class AddressListVC: UIViewController {
    @IBOutlet weak var tblAddress: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var arrAddress = NSMutableArray()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activity.startAnimating()
        refreshControl.addTarget(self, action: #selector(GetAddressList), for: .valueChanged)
        tblAddress.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetAddressList()
    }
}

//MARK:- UITABLEVIEWCELL DELEGATE METHOD
extension AddressListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrAddress.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellAddress = tableView.dequeueReusableCell(withIdentifier: "CellAddress", for: indexPath) as! CellAddress
        let objAddress: CAddress = arrAddress[indexPath.row] as! CAddress
        cell.lblAddress.text = objAddress.address
        cell.lblLatLong.text = "\(objAddress.lattitude ?? 0), \(objAddress.longitude ?? 0)\n\(objAddress.devicetype ?? ""): \(objAddress.deviceuniqueid ?? "")"
        cell.lblDate.text = objAddress.date
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- GET ADDRESS LIST
extension AddressListVC {
     @objc func GetAddressList() {
        let ref = MainRef().child(ADDRESS)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            self.refreshControl.endRefreshing()
            self.activity.stopAnimating()
            if !snapshot.exists() { return }
            self.arrAddress.removeAllObjects()
            self.tblAddress.reloadData()
            let dictAllAddress = snapshot.value! as! NSDictionary
            let arrAllKeys = dictAllAddress.allKeys as NSArray
            for i in 0..<arrAllKeys.count {
                let sKey = arrAllKeys[i] as! String
                let dictAdd: NSDictionary = dictAllAddress.object(forKey: sKey) as! NSDictionary
                print(dictAdd)
                let objAdd: CAddress = CAddress(object: dictAdd)
                if !self.arrAddress.contains(objAdd) {
                    self.arrAddress.add(objAdd)
                }
            }
            let sorted = self.arrAddress.sorted {
                guard let first = ($0 as! CAddress).date else { return false }
                guard let second = ($1 as! CAddress).date else { return true }
                return first.localizedCaseInsensitiveCompare(second) == ComparisonResult.orderedAscending
            }
            self.arrAddress.removeAllObjects()
            for obj in sorted {
                self.arrAddress.insert(obj, at: 0)
            }
            self.tblAddress.reloadData()
        })
    }
}
