//
//  HomeVC.swift
//  COVID
//
//  Created by macOS on 05/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit
import FirebaseAuth
import CoreBluetooth

class HomeVC: UIViewController {

    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var txtDetails: UITextView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    @IBOutlet weak var switchBluetooth: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activity.stopAnimating()
        APP_DELEGATE.GetCurrentLocation()
        lblUserName.text = Auth.auth().currentUser?.displayName ?? "User Name"
        ConnectionManager.sharedInstance.observeReachability()
        BlueToothManager.sharedInstance.InitialzeBluetooth()
        BlueToothManager.sharedInstance.txtDetails = txtDetails
        switchBluetooth.setOn(BlueToothManager.sharedInstance.beeTeeModel.bluetoothIsOn(), animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        activity.stopAnimating()
        switchBluetooth.isOn = false
        BlueToothManager.sharedInstance.beeTeeModel.stopScan()
    }
    
    //MARK:- UIBUTTON ACTION
    @IBAction func switchBluetooth(_ sender: Any) {
        activity.stopAnimating()
        if BlueToothManager.sharedInstance.beeTeeModel.isScanning() {
            BlueToothManager.sharedInstance.beeTeeModel.stopScan()
        } else {
            activity.startAnimating()
            BlueToothManager.sharedInstance.beeTeeModel.startScanForDevices()
        }
    }
    
    @IBAction func btnLogout(_ sender: Any) {
        let alert = UIAlertController(title: "Logout", message: "Are you sure?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { action in
            do { try Auth.auth().signOut() }
            catch { print("already logged out") }
            let vc = loadVC(strStoryboardId: SB_MAIN, strVCId: "navmain")
            UIView.transition(with: APP_DELEGATE.window!, duration: 0.3, options: .transitionCrossDissolve, animations: {
                APP_DELEGATE.window?.rootViewController = vc
            }, completion: nil)
        }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
