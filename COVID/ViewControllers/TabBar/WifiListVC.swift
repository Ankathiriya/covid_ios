//
//  WifiListVC.swift
//  COVID
//
//  Created by macOS on 04/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit

class CellWifi: UITableViewCell {
    @IBOutlet weak var lblWifiName: UILabel!
    @IBOutlet weak var lblUniqueNo: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}

class WifiListVC: UIViewController {
    
    @IBOutlet weak var tblWifi: UITableView!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    var arrWifi = NSMutableArray()
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activity.startAnimating()
        refreshControl.addTarget(self, action: #selector(GetWifiList), for: .valueChanged)
        tblWifi.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        GetWifiList()
    }
}

//MARK:- UITABLEVIEWCELL DELEGATE METHOD
extension WifiListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrWifi.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellWifi = tableView.dequeueReusableCell(withIdentifier: "CellWifi", for: indexPath) as! CellWifi
        let objWifi: CWifi = arrWifi[indexPath.row] as! CWifi
        cell.lblWifiName.text = objWifi.sSID
        cell.lblUniqueNo.text = "\(objWifi.bSSID ?? "")\n\(objWifi.lattitide ?? 0), \(objWifi.longitude ?? 0)\n\(objWifi.devicetype ?? ""): \(objWifi.deviceuniqueid ?? "")"
        cell.lblDate.text = objWifi.date
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- GET BLUETOOTH LIST
extension WifiListVC {
     @objc func GetWifiList() {
        let ref = MainRef().child(WIFI)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            self.refreshControl.endRefreshing()
            self.activity.stopAnimating()
            if !snapshot.exists() { return }
            self.arrWifi.removeAllObjects()
            self.tblWifi.reloadData()
            let dictAllWifi = snapshot.value! as! NSDictionary
            let arrAllKeys = dictAllWifi.allKeys as NSArray
            for i in 0..<arrAllKeys.count {
                let sKey = arrAllKeys[i] as! String
                let dictWifi: NSDictionary = dictAllWifi.object(forKey: sKey) as! NSDictionary
                print(dictWifi)
                let objWifi: CWifi = CWifi(object: dictWifi)
                if !self.arrWifi.contains(objWifi) {
                    self.arrWifi.add(objWifi)
                }
            }
            let sorted = self.arrWifi.sorted {
                guard let first = ($0 as! CWifi).date else { return false }
                guard let second = ($1 as! CWifi).date else { return true }
                return first.localizedCaseInsensitiveCompare(second) == ComparisonResult.orderedAscending
            }
            self.arrWifi.removeAllObjects()
            for obj in sorted {
                self.arrWifi.insert(obj, at: 0)
            }
            self.tblWifi.reloadData()
        })
    }
}
