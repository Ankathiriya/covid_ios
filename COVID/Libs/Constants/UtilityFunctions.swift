//
//  constant.swift

import Foundation
import AVFoundation
import CoreLocation
import UIKit
import Toaster
import Firebase
import FirebaseAuth
import FirebaseDatabase
import SystemConfiguration

var spinner = RPLoadingAnimationView.init(frame: CGRect.zero)
var lat_currnt: Double = 0
var long_currnt: Double = 0

//MARK:-  Get VC for navigation
public func getStoryboard(storyboardName: String) -> UIStoryboard {
    return UIStoryboard(name: storyboardName, bundle: nil)
}

public func loadVC(strStoryboardId: String, strVCId: String) -> UIViewController {
    let vc = getStoryboard(storyboardName: strStoryboardId).instantiateViewController(withIdentifier: strVCId)
    return vc
}

public var CurrentDateTime: String {
    let format = DateFormatter()
    format.dateFormat = "MM/dd/yyyy HH:mm:ss"
    return format.string(from: Date())
}

public func convert(dicData:NSDictionary,toString:(_ strData:String)->())
{
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: dicData)
        if let json = String(data: jsonData, encoding: .utf8) {
            toString(json)
        }
    } catch {
        print("something went wrong with parsing json")
    }
}

public func convert(JSONstring:String,toDictionary:(_ strData:NSDictionary)->())
{
    var strJSON:String = JSONstring
    strJSON = strJSON.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)
    let arr = strJSON.components(separatedBy: "\n")
    var dict : [String:Any]?
    for jsonString in arr{
        if let jsonDataToVerify = jsonString.data(using: String.Encoding.utf8)
        {
            do {
                dict = try JSONSerialization.jsonObject(with: jsonDataToVerify) as? [String : Any]
                print("JSON is valid.")
                toDictionary(dict! as NSDictionary)
            } catch {
                //print("Error deserializing JSON: \(error.localizedDescription)")
            }
        }
    }
}

public func SCREENWIDTH() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.width
}

public func SCREENHEIGHT() -> CGFloat
{
    let screenSize = UIScreen.main.bounds
    return screenSize.height
}

//MARK:- SwiftLoader
public func showLoaderHUD()
{
    let activityData = ActivityData()
    NVActivityIndicatorView.DEFAULT_TYPE = .ballPulse
    NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
    NVActivityIndicatorView.DEFAULT_BLOCKER_MESSAGE = ""
    NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
}

public func hideLoaderHUD()
{
    NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    spinner.removeFromSuperview()
}

public func showMessage(_ bodymsg:String)
{
    ToastView.appearance().backgroundColor = UIColor.black
    ToastView.appearance().font = UIFont.systemFont(ofSize: 14)
    Toast(text: bodymsg).show()
}

//MARK:- Network indicator
public func ShowNetworkIndicator(xx :Bool)
{
    runOnMainThreadWithoutDeadlock {
        UIApplication.shared.isNetworkActivityIndicatorVisible = xx
    }
}

//MARK : Length validation
public func TRIM(string: Any) -> String
{
    return (string as AnyObject).trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
}

public func validateTxtFieldLength(_ txtVal: UITextField, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateTxtViewLength(_ txtVal: UITextView, withMessage msg: String) -> Bool {
    if TRIM(string: txtVal.text ?? "").count == 0
    {
        txtVal.text = TRIM(string: txtVal.text ?? "")
        txtVal.shake()
        showMessage(msg)
        return false
    }
    return true
}

public func validateTextLength(_ strVal: String, withMessage msg: String) -> Bool {
    if TRIM(string: strVal).count == 0
    {
        showMessage(msg)
        return false
    }
    return true
}

public func validateEmailAddress(_ txtVal: UITextField ,withMessage msg: String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    if(emailTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

public func validatePhoneNo(_ txtVal: UITextField ,withMessage msg: String) -> Bool
{
    let PHONE_REGEX = "^[0-9]{6,}$"
    let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
    if(phoneTest.evaluate(with: txtVal.text) != true)
    {
        showMessage(msg);
        return false
    }
    return true
}

func Color_Hex(hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

//MARK:- PRIVATE METHOD
func getAddressFromLatLon(_ pdblLatitude: Double, _ pdblLongitude: Double, complation: @escaping (String) -> Void) {
    let ceo: CLGeocoder = CLGeocoder()
    let loc: CLLocation = CLLocation(latitude:pdblLatitude, longitude: pdblLongitude)
    ceo.reverseGeocodeLocation(loc, completionHandler:
        {(placemarks, error) in
            if (error != nil) { print("reverse geodcode fail: \(error!.localizedDescription)") }
            let pm = placemarks! as [CLPlacemark]
            if pm.count > 0 {
                let pm = placemarks![0]
                var addressString : String = ""
                if pm.subLocality != nil {
                    addressString = addressString + pm.subLocality! + ", "
                }
                if pm.thoroughfare != nil {
                    addressString = addressString + pm.thoroughfare! + ", "
                }
                if pm.locality != nil {
                    addressString = addressString + pm.locality! + ", "
                }
                if pm.country != nil {
                    addressString = addressString + pm.country! + ", "
                }
                if pm.postalCode != nil {
                    addressString = addressString + pm.postalCode! + " "
                }
                print("Current Address:", addressString)
                complation(addressString)
          }
    })
}

//SAVE DATA IN FIREBASE
func MainRef() -> DatabaseReference {
   return Database.database().reference().child(USERS).child(Auth.auth().currentUser?.uid ?? "0")
}

func SaveLocation(_ dictAddress: NSDictionary) {
    let ref = MainRef().child(ADDRESS).childByAutoId()
    ref.setValue(dictAddress)
}

func SaveBlueTooth(_ dictBlueTooth: NSDictionary) {
    let ref = MainRef().child(BLUETOOTH).childByAutoId()
    ref.setValue(dictBlueTooth)
}

func SaveWifi(_ dictWifi: NSDictionary) {
    let ref = MainRef().child(WIFI).childByAutoId()
    ref.setValue(dictWifi)
}

func getUUID() -> String? {
    let keychain = KeychainAccess()
    let uuidKey = "com.my.covidapp.12345"
    if let uuid = try? keychain.queryKeychainData(itemKey: uuidKey) {
        return uuid
    }
    guard let newId = UIDevice.current.identifierForVendor?.uuidString else {
        return nil
    }
    try? keychain.addKeychainData(itemKey: uuidKey, itemValue: newId)
    return newId
}
