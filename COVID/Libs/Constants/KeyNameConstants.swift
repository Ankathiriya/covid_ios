
import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

//MARK:- APPNAME
public let APPNAME :String = "COVID"

//MARK:- LOADER COLOR
let loadercolor = [Color_Hex(hex: "1C2244"), Color_Hex(hex: "131835")]

//MARK:- COLORDS
let APP_COLOR = Color_Hex(hex: "8F6BFF")

//MARK:- STORY BOARD NAMES

public let SB_MAIN:String = "Main"
public let SB_BLUETOOTH:String = "BlueTooth"

//MARK:- FIREBASE KEYS
let USERS = "Users"
let ADDRESS = "Address"
let BLUETOOTH = "Bluetooth"
let WIFI = "Wifi"
let DEVICE_TYPE = "IOS"

//MARK:- USER DEFAULTS KEY
let IS_LOGIN:String = "islogin"
let UD_AUTHTOKEN:String = "UDAUTHTOKEN"
let UD_CUSTOMERID:String = "customerid"
let UD_UserData:String = "UDUserData"
let UD_UserId:String = "UDUserId"
let UD_SecretLogID:String = "UDSecretLogID"
let UD_APIToken:String = "UDUserAccessToken"
let UD_DeviceToken:String = "UDDeviceToken"

//MARK - MESSAGES
let ServerResponseError = "Server Response Error"
let DontHaveCamera = "You don't have camera."
let InternetNotAvail = "Internet Not Available."
let ErrorMSG = "Something going wrong. Please try again!"
