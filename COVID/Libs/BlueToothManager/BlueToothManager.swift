//
//  BlueToothManager.swift
//  COVID
//
//  Created by macOS on 03/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit
import CoreBluetooth

class BlueToothManager: NSObject, BeeTeeDelegate {
    static let sharedInstance = BlueToothManager()
    var txtDetails: UITextView?
    var beeTeeModel = BeeTeeModel.sharedInstance
    var arrBlueTooth: NSMutableArray = NSMutableArray()
    
    func InitialzeBluetooth() {
        beeTeeModel.subscribe(subscriber: self)
    }
    
    func receivedBeeTeeNotification(notification: BeeTeeNotification) {
        for i in 0..<beeTeeModel.availableDevices.count {
            let beeTeeDevice = beeTeeModel.availableDevices[i]
            let dictBlueTooth: NSDictionary = ["Name": TRIM(string: beeTeeDevice.name), "address":TRIM(string: beeTeeDevice.address)]
            if !arrBlueTooth.contains(dictBlueTooth) {
                arrBlueTooth.add(dictBlueTooth)
                let dictTmp: NSMutableDictionary = dictBlueTooth.mutableCopy() as! NSMutableDictionary
                dictTmp.setObject(CurrentDateTime, forKey: "date" as NSCopying)
                dictTmp.setObject(lat_currnt, forKey: "lattitude" as NSCopying)
                dictTmp.setObject(long_currnt, forKey: "longitude" as NSCopying)
                dictTmp.setObject(DEVICE_TYPE, forKey: "devicetype" as NSCopying)
                dictTmp.setObject(APP_DELEGATE.sDeviceUniqueId, forKey: "deviceuniqueid" as NSCopying)
                SaveBlueTooth(dictTmp.copy() as! NSDictionary)
                if txtDetails != nil {
                    txtDetails?.text = "\(txtDetails?.text ?? "")\n\(beeTeeDevice.name) - \(beeTeeDevice.address)"
                }
            }
        }
    }
}
