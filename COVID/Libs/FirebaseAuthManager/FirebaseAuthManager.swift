//
//  FirebaseAuthManager.swift
//  COVID
//
//  Created by macOS on 01/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit
import FirebaseAuth

class FirebaseAuthManager: NSObject {
    func createUser(email: String, password: String, completionBlock: @escaping (_ success: Bool, _ user: User?, _ error: Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) {(authResult, error) in
            if let user = authResult?.user {
                print(user)
                completionBlock(true, authResult?.user, nil)
            } else {
                completionBlock(false, nil, error)
            }
        }
    }
    
    func signIn(email: String, pass: String, completionBlock: @escaping (_ success: Bool, _ user: User?, _ error: Error?) -> Void) {
        Auth.auth().signIn(withEmail: email, password: pass) { (result, error) in
            if let error = error, let _ = AuthErrorCode(rawValue: error._code) {
                completionBlock(false, nil, error)
            } else {
                completionBlock(true, result?.user, nil)
            }
        }
    }
}
