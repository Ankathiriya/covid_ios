//
//  ConnectionManager.swift
//  COVID
//
//  Created by macOS on 03/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit
import Reachability
import SystemConfiguration.CaptiveNetwork

class ConnectionManager: NSObject {
    static let sharedInstance = ConnectionManager()
    var reachability : Reachability!
    var arrWifi: NSMutableArray = NSMutableArray()
    
    func observeReachability(){
        self.reachability = try! Reachability()
        NotificationCenter.default.addObserver(self, selector:#selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
        do {
            try self.reachability.startNotifier()
        }
        catch(let error) {
            print("Error occured while starting reachability notifications : \(error.localizedDescription)")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        let reachability = note.object as! Reachability
        switch reachability.connection {
        case .cellular:
            print("Network available via Cellular Data.")
            break
        case .wifi:
            print("Network available via WiFi.")
            GetWifiInfo()
            break
        case .none:
            print("Network is not available.")
            break
        case .unavailable:
            print("Network is  unavailable.")
            break
        }
    }
    
    func GetWifiInfo() {
        if let interfaces = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces) {
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                if let interfaceData = unsafeInterfaceData as? [String: AnyObject] {
                    let currentSSID = interfaceData["SSID"] as! String
                    let BSSID = interfaceData["BSSID"] as! String
                    let dictWifi: NSDictionary = ["SSID": currentSSID, "BSSID": BSSID]
                    if !arrWifi.contains(dictWifi) {
                        arrWifi.add(dictWifi)
                        let dictTmp: NSMutableDictionary = dictWifi.mutableCopy() as! NSMutableDictionary
                        dictTmp.setObject(CurrentDateTime, forKey: "date" as NSCopying)
                        dictTmp.setObject(lat_currnt, forKey: "lattitide" as NSCopying)
                        dictTmp.setObject(long_currnt, forKey: "longitude" as NSCopying)
                        dictTmp.setObject(DEVICE_TYPE, forKey: "devicetype" as NSCopying)
                        dictTmp.setObject(APP_DELEGATE.sDeviceUniqueId, forKey: "deviceuniqueid" as NSCopying)
                        SaveWifi(dictTmp.copy() as! NSDictionary)
                        print("ssid=\(currentSSID), BSSID=\(BSSID)")
                    }
                }
            }
        }
    }
}

