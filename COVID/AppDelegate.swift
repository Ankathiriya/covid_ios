//
//  AppDelegate.swift
//  COVID
//
//  Created by macOS on 01/09/20.
//  Copyright © 2020 macOS. All rights reserved.
//

import UIKit
import CoreLocation
import Firebase
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var locationManager = CLLocationManager()
    var arrAddress = NSMutableArray()
    var sDeviceUniqueId: String = ""
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
                
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        sDeviceUniqueId = getUUID() ?? ""
        print("UUID: \(sDeviceUniqueId)")
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
}

//MARK:- GET CURRENT LOCATION
extension AppDelegate: CLLocationManagerDelegate {
    func GetCurrentLocation() {
        self.locationManager.requestAlwaysAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.allowsBackgroundLocationUpdates = true
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startMonitoringSignificantLocationChanges()
            AddGeoFencing()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationManager.stopUpdatingLocation()
        let locValue:CLLocationCoordinate2D = manager.location?.coordinate ?? CLLocationCoordinate2D(latitude: 0, longitude: 0)
        lat_currnt = locValue.latitude
        long_currnt = locValue.longitude

        print("Current Lat: ", lat_currnt)
        print("Current Long: ", long_currnt)
        
        if lat_currnt == 0 && long_currnt == 0 { return }
        let dictAddress: NSDictionary = ["lattitude":lat_currnt, "longitude":long_currnt]
        if !self.arrAddress.contains(dictAddress) {
            self.arrAddress.add(dictAddress)
            getAddressFromLatLon(lat_currnt, long_currnt) { (address) in
                let sAddress = TRIM(string: address)
                if sAddress.count > 0 {
                    let dictTmp: NSMutableDictionary = dictAddress.mutableCopy() as! NSMutableDictionary
                    dictTmp.setObject(CurrentDateTime, forKey: "date" as NSCopying)
                    dictTmp.setObject(sAddress, forKey: "address" as NSCopying)
                    dictTmp.setObject(DEVICE_TYPE, forKey: "devicetype" as NSCopying)
                    dictTmp.setObject(APP_DELEGATE.sDeviceUniqueId, forKey: "deviceuniqueid" as NSCopying)
                    SaveLocation(dictTmp.copy() as! NSDictionary)
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location Manager failed with the following error: \(error)")
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        print("StartMonitoring: ",region.identifier)
        locationManager.requestState(for: region)
    }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        if state == .inside {
            print(region.identifier)
        } else {
            print("STOP")
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        let alert = UIAlertController(title: "Alert", message: "Enter Region: \(region.identifier)", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .destructive, handler: nil)
        alert.addAction(action)
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        let alert = UIAlertController(title: "Alert", message: "Exit Region: \(region.identifier)", preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .destructive, handler: nil)
        alert.addAction(action)
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        print("Monitoring failed for region with identifier: \(region!.identifier)")
    }

    func AddGeoFencing() {
        let ref = Database.database().reference().child(USERS)
        ref.observeSingleEvent(of: .value, with: { snapshot in
            if !snapshot.exists() { return }
            let dictAllUsers = snapshot.value as! NSDictionary
            let arrUserKeys = (dictAllUsers.allKeys as NSArray).mutableCopy() as! NSMutableArray
            arrUserKeys.remove(Auth.auth().currentUser?.uid ?? "0")
            print(arrUserKeys)
            for key in arrUserKeys {
                print(key)
                let dictUser = dictAllUsers.object(forKey: key) as! NSDictionary
                let dictAddress = dictUser.object(forKey: ADDRESS) as! NSDictionary
                let arrAllKeys = dictAddress.allKeys as NSArray
                for i in 0..<arrAllKeys.count {
                    let sKey = arrAllKeys[i] as! String
                    let dictAdd: NSDictionary = dictAddress.object(forKey: sKey) as! NSDictionary
                    let objAdd: CAddress = CAddress(object: dictAdd)
                    let geofenceRegionCenter = CLLocationCoordinate2DMake(Double(objAdd.lattitude ?? 0), Double(objAdd.longitude ?? 0))
                    let geofenceRegion = CLCircularRegion(center: geofenceRegionCenter, radius: 100, identifier: sKey)
                    geofenceRegion.notifyOnEntry = true
                    geofenceRegion.notifyOnExit = true
                    self.locationManager.startMonitoring(for: geofenceRegion)
                }
            }
            self.locationManager.startUpdatingLocation()
        })
    }
}

