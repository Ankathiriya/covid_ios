//
//  CWifi.swift
//
//  Created by macOS on 05/09/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CWifi: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let sSID = "SSID"
    static let deviceuniqueid = "deviceuniqueid"
    static let date = "date"
    static let devicetype = "devicetype"
    static let longitude = "longitude"
    static let lattitide = "lattitide"
    static let bSSID = "BSSID"
  }

  // MARK: Properties
  public var sSID: String?
  public var deviceuniqueid: String?
  public var date: String?
  public var devicetype: String?
  public var longitude: Float?
  public var lattitide: Float?
  public var bSSID: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    sSID = json[SerializationKeys.sSID].string
    deviceuniqueid = json[SerializationKeys.deviceuniqueid].string
    date = json[SerializationKeys.date].string
    devicetype = json[SerializationKeys.devicetype].string
    longitude = json[SerializationKeys.longitude].float
    lattitide = json[SerializationKeys.lattitide].float
    bSSID = json[SerializationKeys.bSSID].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = sSID { dictionary[SerializationKeys.sSID] = value }
    if let value = deviceuniqueid { dictionary[SerializationKeys.deviceuniqueid] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    if let value = devicetype { dictionary[SerializationKeys.devicetype] = value }
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = lattitide { dictionary[SerializationKeys.lattitide] = value }
    if let value = bSSID { dictionary[SerializationKeys.bSSID] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.sSID = aDecoder.decodeObject(forKey: SerializationKeys.sSID) as? String
    self.deviceuniqueid = aDecoder.decodeObject(forKey: SerializationKeys.deviceuniqueid) as? String
    self.date = aDecoder.decodeObject(forKey: SerializationKeys.date) as? String
    self.devicetype = aDecoder.decodeObject(forKey: SerializationKeys.devicetype) as? String
    self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? Float
    self.lattitide = aDecoder.decodeObject(forKey: SerializationKeys.lattitide) as? Float
    self.bSSID = aDecoder.decodeObject(forKey: SerializationKeys.bSSID) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(sSID, forKey: SerializationKeys.sSID)
    aCoder.encode(deviceuniqueid, forKey: SerializationKeys.deviceuniqueid)
    aCoder.encode(date, forKey: SerializationKeys.date)
    aCoder.encode(devicetype, forKey: SerializationKeys.devicetype)
    aCoder.encode(longitude, forKey: SerializationKeys.longitude)
    aCoder.encode(lattitide, forKey: SerializationKeys.lattitide)
    aCoder.encode(bSSID, forKey: SerializationKeys.bSSID)
  }

}
