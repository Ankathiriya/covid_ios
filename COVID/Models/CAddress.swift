//
//  CAddress.swift
//
//  Created by macOS on 05/09/20
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class CAddress: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let deviceuniqueid = "deviceuniqueid"
    static let date = "date"
    static let lattitude = "lattitude"
    static let devicetype = "devicetype"
    static let longitude = "longitude"
    static let address = "address"
  }

  // MARK: Properties
  public var deviceuniqueid: String?
  public var date: String?
  public var lattitude: Float?
  public var devicetype: String?
  public var longitude: Float?
  public var address: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    deviceuniqueid = json[SerializationKeys.deviceuniqueid].string
    date = json[SerializationKeys.date].string
    lattitude = json[SerializationKeys.lattitude].float
    devicetype = json[SerializationKeys.devicetype].string
    longitude = json[SerializationKeys.longitude].float
    address = json[SerializationKeys.address].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = deviceuniqueid { dictionary[SerializationKeys.deviceuniqueid] = value }
    if let value = date { dictionary[SerializationKeys.date] = value }
    if let value = lattitude { dictionary[SerializationKeys.lattitude] = value }
    if let value = devicetype { dictionary[SerializationKeys.devicetype] = value }
    if let value = longitude { dictionary[SerializationKeys.longitude] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.deviceuniqueid = aDecoder.decodeObject(forKey: SerializationKeys.deviceuniqueid) as? String
    self.date = aDecoder.decodeObject(forKey: SerializationKeys.date) as? String
    self.lattitude = aDecoder.decodeObject(forKey: SerializationKeys.lattitude) as? Float
    self.devicetype = aDecoder.decodeObject(forKey: SerializationKeys.devicetype) as? String
    self.longitude = aDecoder.decodeObject(forKey: SerializationKeys.longitude) as? Float
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(deviceuniqueid, forKey: SerializationKeys.deviceuniqueid)
    aCoder.encode(date, forKey: SerializationKeys.date)
    aCoder.encode(lattitude, forKey: SerializationKeys.lattitude)
    aCoder.encode(devicetype, forKey: SerializationKeys.devicetype)
    aCoder.encode(longitude, forKey: SerializationKeys.longitude)
    aCoder.encode(address, forKey: SerializationKeys.address)
  }

}
